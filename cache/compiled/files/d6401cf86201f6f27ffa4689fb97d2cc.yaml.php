<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/DaMike/Downloads/mikegauci/user/config/site.yaml',
    'modified' => 1466779580,
    'data' => [
        'title' => 'Free Responsive HTML5/CSS3 Template',
        'author' => [
            'name' => 'Peter Finla'
        ],
        'metadata' => [
            'description' => 'An exclusive HTML5/CSS3 freebie by Peter Finlan, for Codrops.'
        ],
        'header' => [
            'title' => 'I\'m Michael Gauci.',
            'description' => 'I\'m a <span>front-end developer</span>, creating awesome, effective and mobile friendly websites using <span>Boostrap</span>, <span>WordPress</span>, <span>Joomla!</span> and <span>Grav</span>. <a class="smoothscroll" href="#about">Start scrolling</a> and learn more <a class="smoothscroll" href="#about">about me</a>.'
        ],
        'copyright' => [
            0 => [
                'line' => 'Copyright 2014 CeeVee'
            ],
            1 => [
                'line' => 'Design by <a title="Styleshout" href="http://www.styleshout.com/">Styleshout</a>'
            ]
        ],
        'social' => [
            0 => [
                'url' => '#',
                'icon' => 'facebook'
            ],
            1 => [
                'url' => '#',
                'icon' => 'twitter'
            ],
            2 => [
                'url' => '#',
                'icon' => 'google-plus'
            ],
            3 => [
                'url' => '#',
                'icon' => 'linkedin'
            ],
            4 => [
                'url' => '#',
                'icon' => 'instagram'
            ],
            5 => [
                'url' => '#',
                'icon' => 'dribbble'
            ],
            6 => [
                'url' => '#',
                'icon' => 'skype'
            ]
        ],
        'twitter' => [
            'id' => '657554810648948736',
            'domId' => 'tw-widget1',
            'maxTweets' => 3,
            'enableLinks' => 'true',
            'showInteraction' => 'false',
            'showImages' => 'false',
            'showUser' => 'false'
        ]
    ]
];
