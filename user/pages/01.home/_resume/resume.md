---
title: Resume
sections:
    - title: Education
      css_class: education
      items:
        - title: ICE Malta Zebbug
          info: MTA Website Development using ASP.Net
          date: February 2014
        - title: ICE Malta Zebbug
          info: Software Development with C#
          date: November 2014
        - title: ICE Malta Zebbug
          info: MCSD Web Applications
          date: July 2015
    - title: Work
      css_class: work
      items:
        - title: ArenaCube
          info: Junior Front-End Developer
          date: October 2015 - July 2016
        - title: Gaming Innovation Group
          info: Front-End Developer
          date: August 2016 - Present  
    - title: Skills
      css_class: skill
      items:
        - title:
          info:
          date:
          skills:
            - name: HTML5 & CSS3
              level: 100
            - name: Javascript & jQuery
              level: 50
            - name: WordPress
              level: 80
            - name: Joomla!
              level: 80
            - name: Grav
              level: 70
            - name: ASP.Net
              level: 70
            - name: C# and PHP
              level: 40
buttons:
    - url: "http://mikegauciresume.com/"
      icon: file-text-o
      text: View my Resume Online              
---
