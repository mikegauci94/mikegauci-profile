---
title: About
title2: Contact Details
address:
    - line: Flat 8, Cordoba, triq il-Qawra
    - line: San Pawl il-Bahar SPB 1904
    - line: +356 99256511
email:
    - address: mikegauci@gmail.com
buttons:
    - url: "http://mikegauciresume.com/"
      icon: file-text-o
      text: View my Resume Online
---
## About Me

In 2014 I decided that I wasn't going to live my life without having a decent education and a career for my future

I was encouraged to study in the IT area since its a growing subject and a career in demand, so I looked up some courses and I found a course which appealed to me on icemalta.com which was MTA Website Development with ASP.NET. As soon as the first lesson passed, my passion for web development and coding started to grow.
