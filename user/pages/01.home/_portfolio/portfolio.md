---
title: Portfolio
portfolio:
  - title: ArenaLine
    description:
    img: arenaline-1.png
    content: 'At ArenaCube I was asked to build one of their corporate websites using Grav "A Modern Flat-File CMS", a new technology that they introduced. With the help of the designer who was providing me with mock ups, ArenaLine was the first website I used in Grav.'
    tags: Grav, Bootstrap, jQuery
    details: http://www.arenaline.com
  - title: "Resume"
    description: 
    img: resume.png
    content: "This is my online resume/cv which I showcase my skills, education, courses, online courses experience and more."
    tags: Grav, jQuery
    details: http://mikegauciresume.com
  - title: "Capital City Suites"
    img: vallettacitysuites-1.png
    content: "Capital City Suites is an online booking accommodation that a client asked me to create for her flat in Valletta. I also had to sync her calenders from Booking.com and Airbnb to sync with the website."
    tags: Wordpress, Bootstrap, jQuery
    details: http://www.capitalcitysuites.com
  - title: "Bla Senserija"
    img: blasenserija-1.png
    content: "This website was built for real estate, where sellers submit their property to the public and buyers can view their desired property without having the agent in between."
    tags: Wordpress, jQuery
    details: http://www.blasenserija.com
  - title: "Websedia"
    img: websedia-1.png
    content: "Websedia is a Web Design and Development agency where their services is based on building, hosting and branding websites."
    tags: Wordpress, Bootstrap, jQuery
    details: http://www.websedia.com
  - title: "Peking Restaurant"
    description: 
    img: peking.png
    content: "This was my first real website. My former boss suggested to practice web development by building them a website for their restaurant."
    tags: ASP.Net, Bootstrap, jQuery
    details: http://mikegauci.github.io/peking/
  - title: "My Blog"
    description: 
    img: blog.png
    content: "This was my first website to showcase my work and starting a portfolio."
    tags: Bootstrap, jQuery
    details: http://mikegauci.github.io/mike-blog/
  - title: "Gamma Photo Sharing"
    description: Project Based Site
    img: gamma.png
    content: "As my skills started to grow slowly I wanted to learn how to build more Bootstrap styled websites"
    tags: Bootstrap, jQuery
    details: http://mikegauci.github.io/gamma/
  - title: "Tech Edge"
    description:  Project Based Site
    img: tech-edge.png
    content: This website was also built with the help of another <a href="https://www.udemy.com/learn-bootstrap-development-by-building-10-projects/">Udemy course</a>
    tags: Bootstrap, jQuery
    details: http://mikegauci.github.io/tech-edge/
  - title: "Magazine Layout"
    description:
    img: magazine.png
    content: This was one of my second project based websites with the same <a href="https://www.udemy.com/learn-to-build-beautiful-html5-and-css3-websites-in-1-month/">Udemy course </a> I had participated in with the same instructor as my second website with a magazine styled approach.
    tags: Bootstrap, jQuery
    details: http://mikegauci.github.io/magazine/
  - title: "One Million Lines"
    description: Project Based Site
    img: oml.png
    content: This was my first ever attempt in building a website which was a project based, where I needed to start applying my skills I had learned in theory and with help of an online course at <a href="https://www.udemy.com/learn-to-build-beautiful-html5-and-css3-websites-in-1-month/">Udemy</a> my love for web development grew.
    tags: Bootstrap, jQuery
    details: http://mikegauci.github.io/oml/

---
##Portfolio
